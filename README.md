# Methods for Numerical Mathematics ( Methoden zur Numerischen Mathematik )


## Badges & Links to Deployments
* [![Binderhub](https://img.shields.io/badge/Binderhub-Jupyterlab-orange)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fsamira.altpeter%2Fmethodsnm/HEAD)
* [![lite-badge](https://img.shields.io/badge/Jupyterlite-yellow)](https://samira.altpeter.pages.gwdg.de/methodsnm/jl)
* [![static-html](https://img.shields.io/badge/static_HTMLs-white)](https://samira.altpeter.pages.gwdg.de/methodsnm/static.html)
* [![Docker](https://img.shields.io/badge/Dockerhub-blue)](https://gitlab.gwdg.de/samira.altpeter/methodsnm/container_registry)
* [![API-docu](https://img.shields.io/badge/PyDoc-purple)](https://samira.altpeter.pages.gwdg.de/methodsnm/doc/methodsnm.html)



### Configuring CI/CD in GWDG gitlab:
Once you have forked this repository your own GWDG Gitlab project should automatically have CI/CD and gitlab pages deployment, container registry, etc.. active in its project settings so it should be able to run the CI routines for enabling Jupyterlite. 
The links in this README.md should however be updated to your own project's path (replace `lehrenfeld` with your namespace).

