from abc import ABC, abstractmethod
import numpy as np
from numpy import array, einsum, transpose
from methodsnm.intrule import *
from methodsnm.fe import *
from numpy.linalg import det, inv
from methodsnm.meshfct import ConstantFunction

class FormIntegral(ABC):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")


class LinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_vector(self, fe, trafo):
        raise NotImplementedError("Not implemented")


class SourceIntegral(LinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_vector(self, fe, trafo, intrule = None):

        
        if intrule is None:
            intrule = select_integration_rule(fe.order, fe.eltype)
        
        quad_points = intrule.nodes
        
        coeffs = self.coeff.evaluate(quad_points, trafo)
        weights = intrule.weights
        
        deter = [abs(det(trafo.jacobian(quad_points[i]))) for i in range(len(quad_points))]
        
        # Phi und Anzahl der phi's:
        shapes = fe.evaluate(quad_points)
        dof = fe.ndof
        
        fs = []
        for i in range(dof):
            phi = shapes[:,i]
            
            # tatsächliches Integral:
            integral = 0
            for k in range(len(quad_points)):
                
                integral += weights[k] * coeffs[k] * deter[k] * phi[k]
                
            fs.append(integral)
        
        return fs


class BilinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_matrix(self, fe, trafo):
        raise NotImplementedError("Not implemented")


class MassIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff


    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
     
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
        
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        quad_points = intrule.nodes
        
        shapes_test = fe_test.evaluate(quad_points)
        shapes_trial = fe_trial.evaluate(quad_points)
        
        coeffs = self.coeff.evaluate(quad_points, trafo)
        weights = intrule.weights
        
        deter = [abs(det(trafo.jacobian(quad_points[i]))) for i in range(len(quad_points))]
        
        dof_test = fe_test.ndof
        dof_trial = fe_trial.ndof
        
        M_T = np.zeros((dof_test, dof_trial))
        
        for i in range(dof_test):
            phi = shapes_test[:,i]
            
            for j in range(dof_trial):
                
                psi = shapes_trial[:,j]
                
                summe = 0
                for k in range(len(coeffs)):

                    summe += weights[k] * coeffs[k] * deter[k] * phi[k] * psi[k]
                
                M_T[i, j] = summe  
        
        return M_T



class LaplaceIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):

        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
        
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        quad_points = intrule.nodes
        
   
        shapes_test_deriv = fe_test.evaluate(quad_points, deriv = True) 
        shapes_trial_deriv = fe_trial.evaluate(quad_points, deriv = True) 
        
        Phi_T = trafo.jacobian(quad_points)
        Phi_T_inv = array([inv(Phi_T[i,:,:]) for i in range(Phi_T.shape[0])])
        Phi_T_det = array([abs(det(Phi_T[i,:,:])) for i in range(Phi_T.shape[0])])

        coeffs = self.coeff.evaluate(quad_points, trafo)
        weights = intrule.weights
        
        
        dof_test = fe_test.ndof
        dof_trial = fe_trial.ndof
           
        laplace = einsum("ijk,imn,ijl,iml,i,i,i->kn", shapes_test_deriv, shapes_trial_deriv, Phi_T_inv, Phi_T_inv, Phi_T_det, coeffs, weights)
        
        return laplace


