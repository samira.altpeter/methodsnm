from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.trafo import *
from methodsnm.mesh import *
from methodsnm.intrule import *
from scipy import sparse
import numpy as np

class Form(ABC):
    integrals = None
    fes = None
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def add_integrator(self, integrator):
        self.integrals.append(integrator)

    def __iadd__(self, integrator):
        self.add_integrator(integrator)
        return self

class LinearForm(Form):

    vector = None
    def __init__(self, fes=None):
        self.fes = fes
        self.integrals = []
    
    def assemble(self):
        self.vector = np.zeros(self.fes.ndof)
        mesh = self.fes.mesh
        for elnr, verts in enumerate(mesh.elements()):
            trafo = mesh.trafo(elnr)
            fe = self.fes.finite_element(elnr)
            dofs = self.fes.element_dofs(elnr)
            for integral in self.integrals:
                self.vector[dofs] += integral.compute_element_vector(fe, trafo)


class BilinearForm(Form):

    matrix = None
    def __init__(self, fes=None, fes_test=None, fes_trial=None):
        if fes is None and (fes_test is None or fes_trial is None) or all([f is not None for f in [fes,fes_test,fes_trial]]):
            raise Exception("Invalid arguments, specify either `fes` or `fes_test` and `fes_trial`")
        if fes_test is None:
            fes_test = fes
            fes_trial = fes

        self.fes_trial = fes_trial
        self.fes_test = fes_test
        self.fes = fes_test
        self.integrals = []
    
    def assemble(self):
        self.matrix = sparse.lil_matrix((self.fes_test.ndof, self.fes_trial.ndof))
        mesh = self.fes.mesh
        for elnr, verts in enumerate(mesh.elements()):
            trafo = mesh.trafo(elnr)
            fe_test = self.fes_test.finite_element(elnr)
            dofs_test = self.fes_test.element_dofs(elnr)
            fe_trial = self.fes_trial.finite_element(elnr)
            dofs_trial = self.fes_trial.element_dofs(elnr)
            elmat = np.zeros((len(dofs_test), len(dofs_trial)))
            
            for integral in self.integrals:
                elmat += integral.compute_element_matrix(fe_test, fe_trial, trafo)
            for i, dofi in enumerate(dofs_test):
                for j, dofj in enumerate(dofs_trial):
                    self.matrix[dofi,dofj] += elmat[i,j]
        self.matrix = self.matrix.tocsr()




def error_L2(u_h, u_exact, intrule = None):

    fes = u_h.fes
    mesh = fes.mesh
    
    err = 0
    for elnr in range(len((mesh.elements()))):
        trafo = mesh.trafo(elnr)
        fe = fes.finite_element(elnr)
    
        if intrule is None:
            intrule = select_integration_rule(fe.order * 2, fe.eltype)
    
        quad_points = intrule.nodes
    
        uh_vals = u_h.evaluate(quad_points, trafo)
        uexact_vals = u_exact.evaluate(quad_points, trafo)
        
        diff = np.zeros(len(quad_points))
        diff = (uh_vals - uexact_vals)**2

        weights = intrule.weights
        F = trafo.jacobian(intrule.nodes)
        deter = array([abs(np.linalg.det(F[i,:,:])) for i in range(F.shape[0])])
  
        for k in range(len(diff)):

            err += deter[k] * diff[k] * weights[k]

    return np.sqrt(err)

"""
def error_H1(u_h, u_exact, deriv_u_exact, intrule = None):

    fes = u_h.fes
    mesh = fes.mesh
    
    err = 0
    for elnr in range(len((mesh.elements()))):
        trafo = mesh.trafo(elnr)
        fe = fes.finite_element(elnr)
    
        if intrule is None:
            intrule = select_integration_rule(fe.order * 2, fe.eltype)
    
        quad_points = intrule.nodes
    
        uh_vals = u_h.evaluate(quad_points, trafo)
        uexact_vals = u_exact.evaluate(quad_points, trafo)
        
        uh_vals_deriv = u_h.evaluate(quad_points, trafo, deriv = True)
        uexact_vals_deriv = deriv_exact.evaluate(quad_points, trafo)
        
        diff = np.zeros(len(quad_points))
        diff_deriv = np.zeros(len(quad_points))
        diff = (uh_vals - uexact_vals)**2
        diff_deriv = (uh_vals_deriv - uexact_vals_deriv)**2
        print("DERIVATIVE", diff_deriv)

        weights = intrule.weights
        F = trafo.jacobian(intrule.nodes)
        deter = array([abs(np.linalg.det(F[i,:,:])) for i in range(F.shape[0])])
  
        for k in range(len(diff)):

            err += deter[k] * diff[k] * weights[k] #+ deter[k] * diff_deriv[k] * weights[k]

    return np.sqrt(err)
"""

