"""
This module provides classes for numerical integration rules in 2D (triangles).
"""

from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.intrule import IntRule

class IntRuleTriangle(IntRule):
    """
    Abstract base class for numerical integration rules on triangle.
    """
    def __init__(self):
        pass

class EdgeMidPointRule(IntRuleTriangle):
    """
    Class for the midpoint rule for 1D numerical integration.
    """
    def __init__(self):
        """
        Initializes the midpoint rule with the given interval.

        """
        self.nodes = array([[0.5,0.0],[0.5,0.5],[0.0,0.5]])
        self.weights = array([1.0/6.0,1.0/6.0,1.0/6.0])
        self.exactness_degree = 1

        

class NewtonCotesRule_2D(IntRule):
    """
    Class for the Newton-Cotes rule for 2D numerical integration.
    """
    def __init__(self, n_x =None, n_y =None, nodes_x=None, nodes_y=None, interval_x=(0,1), interval_y=(0,1)):
        """
        Initializes the Newton-Cotes rule with the given interval and number of nodes.

        Parameters:
        n (int or list): The number of nodes or a list of nodes to use for integration.
        nodes (list): A list of nodes to use for integration.
        interval (tuple): The interval to integrate over.
        """
        self.interval_x = interval_x
        a_x,b_x = interval_x
        
        self.interval_y = interval_y
        a_y,b_y = interval_y
        
        if nodes_x is None and nodes_y is None and n_x is None and n_y is None:
            raise ValueError("Either n or nodes must be specified")
        if isinstance(n_x, list) and isinstance(n_y, list):
            nodes_x = n_x
            nodes_y = n_y
        if isinstance(n_x, list) and isinstance(n_y, int):
            nodes_x = n_x
            nodes_y = np.linspace(a_y,b_y,n_y)
        if isinstance(n_x, int) and isinstance(n_y, list):
            nodes_x = np.linspace(a_x,b_x,n_x)
            nodes_y = n_y
        else:
            nodes_x = np.linspace(a_x,b_x,n_x)
            nodes_y = np.linspace(a_y,b_y,n_y)
        n_x = len(nodes_x)
        n_y = len(nodes_y)

        self.nodes_x = array(nodes_x)
        self.nodes_y = array(nodes_y)

        # Compute weights:
        weights_help = []
        

        self.weights = array(weights_help)



from methodsnm.intrule_1d import NP_GaussLegendreRule
from methodsnm.intrule_1d import SP_GaussJacobiRule
class DuffyBasedRule(IntRule):
    def __init__(self, order):
        gp_points = max(order,0)//2+1
        self.gauss = NP_GaussLegendreRule(gp_points)
        self.gjacobi = SP_GaussJacobiRule(gp_points,alpha=1,beta=0)
        self.nodes = array([[(1-eta[0])*xi[0], eta[0]] for xi in self.gauss.nodes for eta in self.gjacobi.nodes])
        self.weights = array([w1*w2 for w1 in self.gauss.weights for w2 in self.gjacobi.weights])
        self.exactness_degree = 2*gp_points-1

